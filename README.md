# Trello to XLSX

Convert Trello board's JSON to XLSX file for further processing data.

## Development start

Requirements: python 3.8+, poetry 1.0+.

- `poetry shell` - enter to virtual environment
- `poetry install` - install dependencies
- `python main.py` - start development server
- open http://localhost:5000

## Docker

- `docker build -t trello-to-xlsx .` - build docker imag
- `docker run -ti -p 5000:5000 trello-to-xlsx` - start docker image on port 5000
