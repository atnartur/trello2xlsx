import tempfile
from datetime import datetime

from openpyxl import Workbook
from openpyxl.cell import WriteOnlyCell
from openpyxl.styles import Font


def get_creation_date_from_id(id: str):
    return datetime.fromtimestamp(int(id[0:8], 16))


def parse_board(data):
    json_cards = data["cards"]

    lists = {list["id"]: list["name"] for list in data["lists"]}
    labels = {label["id"]: label["name"] for label in data["labels"]}
    members = {member["id"]: member["fullName"] for member in data["members"]}

    fieldnames = [
        "Task name",
        "Assignee",
        "Created at",
        "Updated at",
        "List",
        "Labels",
        "Link",
    ]

    wb = Workbook(write_only=True)

    ws = wb.create_sheet()
    cells = []
    for field in fieldnames:
        cell = WriteOnlyCell(ws, value=field)
        cell.font = Font(bold=True)
        cells.append(cell)
    ws.append(cells)

    for json_card in json_cards:
        assignee = ''
        members_count = len(json_card["idMembers"])
        if members_count == 1:
            assignee = members[json_card["idMembers"][0]]
        elif members_count > 1:
            str_members = ""
            for member in json_card["idMembers"]:
                str_members += members[member] + ", "
            assignee = str_members

        label = ""
        labels_count = len(json_card["labels"])
        if labels_count == 1:
            label = labels[json_card["labels"][0]["id"]]
        elif labels_count > 1:
            str_labels = ""
            for label in json_card["labels"]:
                str_labels += labels[label["id"]] + ", "
            label = str_labels

        ws.append([
            json_card["name"],
            assignee,
            get_creation_date_from_id(json_card["id"]),
            json_card["dateLastActivity"].replace("T", " "),
            lists[json_card["idList"]],
            label,
            json_card["shortUrl"]
        ])

    with tempfile.NamedTemporaryFile(suffix='.xlsx', delete=False) as f:
        wb.save(f.name)
    return f
