FROM python:3.10-slim

ENV FLASK_ENV=production
ENV FLASK_CONFIGURATION=production
ENV FLASK_APP=app.py

RUN apt update && apt install -y --no-install-recommends libffi-dev gcc python3-dev && \
    pip install --upgrade pip

WORKDIR /app
ENV PYTHONUNBUFFERED 1

RUN pip install --upgrade pip poetry==1.0.5 && poetry config virtualenvs.create false

COPY pyproject.toml .
COPY poetry.lock .
RUN poetry install --no-dev
COPY . .
COPY uwsgi.ini .

ENV PORT 5000

CMD uwsgi --ini uwsgi.ini --callable app --http-socket :$PORT
