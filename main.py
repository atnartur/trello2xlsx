import json
from json import JSONDecodeError

from flask import Flask, request, send_file

from services import parse_board

app = Flask(__name__)


@app.route("/")
def main():
    with open("index.html", 'r') as f:
        return f.read()


@app.route("/process", methods=['POST'])
def process():
    if 'file' not in request.files:
        return "<p>No file uploaded</p><a href='/'>Return</a>", 400
    try:
        file = request.files['file']
        data = json.loads(file.stream.read())
        file = parse_board(data)
    except JSONDecodeError:
        return "<p>JSON decode error</p><a href='/'>Return</a>", 400
    except BaseException:
        return "<p>Unknown error occured</p><a href='/'>Return</a>", 500
    return send_file(
        file.name,
        mimetype='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
        download_name=f'trello.xlsx',
        as_attachment=True
    )


if __name__ == '__main__':
    app.run(host='0.0.0.0')
